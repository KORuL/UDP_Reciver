#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    this->setWindowTitle(QString::fromUtf8("UDP Reciver"));

    loadParam();

    start                       = false;
    byteOrder                   = false;
    SaveTo_Filetrue_IPfalse     = false;

    Allsize   = 0;

    SocRecive   = new QUdpSocket(this);
    SocTransmit = new QUdpSocket(this);


    idealThreadCnt = QThread::idealThreadCount();
    func = new QFuture<void>[idealThreadCnt];


    QObject::connect(SocRecive,   SIGNAL(disconnected()), this, SLOT(disconnectedR()));
    QObject::connect(SocTransmit, SIGNAL(disconnected()), this, SLOT(disconnectedT()));

    QObject::connect(SocRecive,   SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(errorR(QAbstractSocket::SocketError)));
    QObject::connect(SocTransmit, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(errorT(QAbstractSocket::SocketError)));

    QObject::connect(SocRecive,   SIGNAL(readyRead()),    this, SLOT(readyReadSlot()), Qt::DirectConnection);

    IN .resize(32840);
    OUT.resize(32840);

    if(ui->LE_OutputFile->text() == " ")
    {
        QString dirEtal  = QCoreApplication::applicationDirPath();
        dirEtal += "/Out";

        QDir dir1(dirEtal);
        if(!dir1.exists())
            dir1.mkdir(dirEtal);

        dirEtal += "/1.bin";

        file.setFileName(dirEtal);
        file.open(QIODevice::Append | QIODevice::WriteOnly);

        ui->LE_OutputFile->setText(dirEtal);
        STRFile = dirEtal;
    }

    ui->LE_InIP     ->setEnabled(true);
    ui->CB_byteOrder->setEnabled(true);
}

MainWindow::~MainWindow()
{
    saveParam();

    delete SocRecive  ;
    delete SocTransmit;

    delete ui;
}


void MainWindow::closeEvent(QCloseEvent *)
{
    saveParam();

    on_PB_Stop_2_clicked ();
}

void MainWindow::on_PB_ChooseFile_clicked()
{
    QString str;
    str = QFileDialog::getOpenFileName(0, "Load", "", "");

    ui->LE_OutputFile->setText(str);

    STRFile = str;

    if(file.isOpen())
        file.close();

    file.setFileName(STRFile);
    file.resize(0);
    file.open(QIODevice::Append | QIODevice::WriteOnly);
}

void MainWindow::PB_ConectionRecieve_clicked()
{
    QString Port;
    bool    ok;
    quint16 port;

    Port = ui->LE_InputPort_2->text();
    port = Port.toInt(&ok, 10);

    if(ok)
    {
        bool er = false;

        if(!ui->CB_Multicast->isChecked())
        {
            er = SocRecive->bind(QHostAddress::Any, port, QUdpSocket::ShareAddress);
        }
        else
        {
            bool er1 = false;
            QString IP;
            IP = ui->LE_InIP->text();

            er  = SocRecive->bind(QHostAddress::AnyIPv4, port, QUdpSocket::ShareAddress);
            er1 = SocRecive->joinMulticastGroup(QHostAddress(IP));

            er = er | er1;
        }

        if(er)
        {
            ui->LE_InputPort_2->setEnabled(false);

            Log(QString("Connected to Recieved"));
        }
        else
        {
            ui->LE_InputPort_2->setEnabled(true);

            Log(QString("Not Connected"));
        }
    }
    else
    {
        Log(QString("Port recieved is not a number"));
    }
}

void MainWindow::on_CB_Multicast_clicked()
{
//    if(ui->CB_Multicast->isChecked())
//    {
//        ui->LE_InIP->setEnabled(true);
//    }
//    else
//    {
//        ui->LE_InIP->setEnabled(false);
//    }
}

void MainWindow::on_CB_byteOrder_clicked()
{
    if(ui->CB_byteOrder->isChecked())
    {
        byteOrder = true;
    }
    else
    {
        byteOrder = false;
    }
}

void MainWindow::on_RB_SendToIP_clicked()
{
    if(ui->RB_SendToIP->isChecked())
        SaveTo_Filetrue_IPfalse = false;
}

void MainWindow::on_RB_SaveFile_clicked()
{
    if(ui->RB_SaveFile->isChecked())
        SaveTo_Filetrue_IPfalse = true;
}


void MainWindow::on_PB_clear_clicked()
{
    ui->TE_LOG_2->clear();
}


void MainWindow::PB_ConectionTransmit_clicked()
{
    QString Port;
    bool    ok;
    int     port;

    Port = ui->LE_OutputPort_2->text();
    port = Port.toInt(&ok, 10);

    if(ok)
    {
        ui->LE_OutputIP_2  ->setEnabled(false);
        ui->LE_OutputPort_2->setEnabled(false);
    }
    else
    {
        Log(QString("Port transmit is not a number"));
    }
}

void MainWindow::on_PB_Start_2_clicked()
{
    if ( SocRecive->state() != QAbstractSocket::BoundState )
        PB_ConectionRecieve_clicked();

    PB_ConectionTransmit_clicked();

    if(SocRecive->state() == QAbstractSocket::BoundState)
    {
        start   = true;
        Allsize = 0;

        if(SaveTo_Filetrue_IPfalse)
        {
            if(ui->LE_OutputFile->text() != STRFile)
            {
                file.close();

                STRFile = ui->LE_OutputFile->text();
                file.setFileName(STRFile);
            }

            file.open(QIODevice::Append | QIODevice::WriteOnly);
            file.resize(0);
        }

        dstLen    = 0;

        Log(QString(" Start "));

        ui->PB_Start_2         ->setEnabled(false);
        ui->PB_ChooseFile      ->setEnabled(false);
        ui->RB_SendToIP        ->setEnabled(false);
        ui->RB_SaveFile        ->setEnabled(false);
        ui->LE_OutputFile      ->setEnabled(false);
        ui->CB_byteOrder       ->setEnabled(false);
        ui->CB_Multicast       ->setEnabled(false);
        ui->LE_InIP            ->setEnabled(false);
    }
    else
    {
        Log(QString(" Recieve or Transmit connections are not established "));
        on_PB_Stop_2_clicked();
    }
}


void MainWindow::on_PB_Stop_2_clicked()
{
    SocTransmit->close();
    SocRecive  ->close();

    ui->LE_OutputIP_2  ->setEnabled(true);
    ui->LE_OutputPort_2->setEnabled(true);
    ui->LE_InputPort_2 ->setEnabled(true);
    ui->PB_ChooseFile  ->setEnabled(true);
    ui->RB_SendToIP    ->setEnabled(true);
    ui->RB_SaveFile    ->setEnabled(true);
    ui->LE_OutputFile  ->setEnabled(true);
    ui->CB_Multicast   ->setEnabled(true);
    ui->LE_InIP        ->setEnabled(true);
    ui->CB_byteOrder   ->setEnabled(true);

    start = false;

    if(file.isOpen())
        file.close();

    Log(QString("Stoped"));
    Log(QString("Обработано %1 байт").arg(Allsize));

    ui->PB_Start_2->setEnabled(true);
}

void MainWindow::connectedR()
{
    Log(QString("Connected to Recieved"));
    ui->LE_InputPort_2->setEnabled(false);
    ui->LE_InIP       ->setEnabled(false);
}

void MainWindow::connectedT()
{
    Log(QString("Connected to Transmit"));
    ui->LE_OutputIP_2  ->setEnabled(false);
    ui->LE_OutputPort_2->setEnabled(false);
}

void MainWindow::disconnectedR()
{
    Log(QString("Disconnected to Recieved"));
}

void MainWindow::disconnectedT()
{
    Log(QString("Disconnected to Transmit"));
}

void MainWindow::saveParam()
{
    ///< сохраняем настройки
    QSettings settings("KORuL", "UDP Reciver");
    ///< запомним геометрию и расположение
    settings.setValue("geometry_mainwindow", geometry());
    ///< запомним путь
    settings.setValue("IPT",        ui->LE_OutputIP_2  ->text());
    settings.setValue("IPR",        ui->LE_InIP        ->text());
    settings.setValue("PortR",      ui->LE_InputPort_2 ->text());
    settings.setValue("PortT",      ui->LE_OutputPort_2->text());
    settings.setValue("FileName",   ui->LE_OutputFile  ->text());

    settings.sync(); ///< записываем настройки
}

void MainWindow::loadParam()
{
    QSettings settings("KORuL", "UDP Reciver");
    ///< считаем геометрию и расположение и установим
    QRect rect = settings.value("geometry_mainwindow", QRect(200,200,400,320)).toRect();
    this->setGeometry(rect);

    ui->LE_OutputIP_2  ->setText(settings.value("IPT",     "127.0.0.1").toString());
    ui->LE_InIP        ->setText(settings.value("IPR",     "127.0.0.1").toString());
    ui->LE_InputPort_2 ->setText(settings.value("PortR",   "5000").toString());
    ui->LE_OutputPort_2->setText(settings.value("PortT",   "5000").toString());
    ui->LE_OutputFile  ->setText(settings.value("FileName"," ").toString());

    if(ui->LE_OutputFile->text() != " ")
    {
        file.setFileName(ui->LE_OutputFile->text());
        file.open(QIODevice::Append | QIODevice::WriteOnly);

        STRFile = ui->LE_OutputFile->text();
    }
}


void MainWindow::errorR(QAbstractSocket::SocketError error)
{
    QString err;
    err += "Recieve error: ";
    Error(err, error);
}

void MainWindow::errorT(QAbstractSocket::SocketError error)
{
    QString err;
    err += "Transmit error: ";
    Error(err, error);
}



void MainWindow::Log(QString str)
{
    QString message;

    message = "-   " + QTime::currentTime().toString() + "  " + str + "     ";

    ui->TE_LOG_2->append(message);
}

void MainWindow::Error(QString &err, QAbstractSocket::SocketError error)
{
    on_PB_Stop_2_clicked();

    switch(error)
    {
    case 0:
        Log(err.append("The connection was refused by the peer (or timed out)."));
        break;
    case 1:
        Log(err.append("The remote host closed the connection. Note that the client socket (i.e., this socket) will be closed after the remote close notification has been sent."));
        break;
    case 2:
        Log(err.append("The host address was not found."));
        break;
    case 3:
        Log(err.append("The socket operation failed because the application lacked the required privileges."));
        break;
    case 4:
        Log(err.append("The local system ran out of resources (e.g., too many sockets)."));
        break;
    case 5:
        Log(err.append("The socket operation timed out."));
        break;
    case 6:
        Log(err.append("The datagram was larger than the operating system's limit (which can be as low as 8192 bytes)."));
        break;
    case 7:
        Log(err.append("An error occurred with the network (e.g., the network cable was accidentally plugged out)."));
        break;
    case 8:
        Log(err.append("The address specified to QAbstractSocket::bind() is already in use and was set to be exclusive."));
        break;
    case 9:
        Log(err.append("The address specified to QAbstractSocket::bind() does not belong to the host."));
        break;
    case 10:
        Log(err.append("The requested socket operation is not supported by the local operating system (e.g., lack of IPv6 support)."));
        break;
    case 11:
        Log(err.append("The socket is using a proxy, and the proxy requires authentication."));
        break;
    case 12:
        Log(err.append("The SSL/TLS handshake failed, so the connection was closed (only used in QSslSocket)"));
        break;
    case 13:
        Log(err.append("Used by QAbstractSocketEngine only, The last operation attempted has not finished yet (still in progress in the background)."));
        break;
    case 14:
        Log(err.append("Could not contact the proxy server because the connection to that server was denied"));
        break;
    case 15:
        Log(err.append("The connection to the proxy server was closed unexpectedly (before the connection to the final peer was established)"));
        break;
    case 16:
        Log(err.append("The connection to the proxy server timed out or the proxy server stopped responding in the authentication phase."));
        break;
    case 17:
        Log(err.append("The proxy address set with setProxy() (or the application proxy) was not found."));
        break;
    case 18:
        Log(err.append("The connection negotiation with the proxy server failed, because the response from the proxy server could not be understood."));
        break;
    case 19:
        Log(err.append("An operation was attempted while the socket was in a state that did not permit it."));
        break;
    case 20:
        Log(err.append("The SSL library being used reported an internal error. This is probably the result of a bad installation or misconfiguration of the library."));
        break;
    case 21:
        Log(err.append("Invalid data (certificate, key, cypher, etc.) was provided and its use resulted in an error in the SSL library."));
        break;
    case 22:
        Log(err.append("A temporary error occurred (e.g., operation would block and socket is non-blocking)."));
        break;
    case -1:
        Log(err.append("An unidentified error occurred."));
        break;

    default:
        ;
    }
}


////////////////////////////////////
/// \brief MainWindow::readyReadSlot
///
///
///
///
///
///


void MainWindow::readyReadSlot()
{
    if(start)
    {
        while ( SocRecive->hasPendingDatagrams() )
        {
            int available = SocRecive->bytesAvailable();

            QHostAddress address;
            quint16      port;

            available = SocRecive->readDatagram(IN.data(), available, &address, &port);

            if(byteOrder)
            {
                if(funcWrite.isRunning())
                    funcWrite.waitForFinished();

                for(int i = 0; i < available; i++)
                {
                    if(i == 0 || i%2 == 0)
                        OUT[i] = IN[i+1];
                    else
                        OUT[i] = IN[i-1];
                }
            }

            if(SaveTo_Filetrue_IPfalse)
            {
                if(!byteOrder && funcWrite.isRunning())
                    funcWrite.waitForFinished();

                funcWrite = QtConcurrent::run(this, &MainWindow::SendToFile, available);
            }
            else
            {
                SendToIP(available);
            }

        }
    }
}


void MainWindow::SendToFile(int size)
{    
    quint64 size1;

    if(byteOrder)
        size1 = file.write(OUT.data(), size  );
    else
        size1 = file.write(IN .data(), size  );

    Allsize += size1;
}


void MainWindow::SendToIP(int size)
{
    QString IP     = ui->LE_OutputIP_2  ->text();
    QString Port   = ui->LE_OutputPort_2->text();
    quint16 port   = Port.toInt();
    quint64 size1;

    if(byteOrder)
        size1 = SocTransmit->writeDatagram(OUT.data(), size, QHostAddress(IP), port);
    else
        size1 = SocTransmit->writeDatagram(IN.data(),  size, QHostAddress(IP), port);

    Allsize += size1;
}
