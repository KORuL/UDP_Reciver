#-------------------------------------------------
#
# Project created by QtCreator 2016-08-16T08:43:06
#
#-------------------------------------------------

if(contains(QMAKE_HOST.arch, x86_64)) {

   win32-msvc* {
       MSVC_VER = $$(VisualStudioVersion)
       equals(MSVC_VER, 14.0) {
          Release:DESTDIR = d:/qwt-6.1.3/examples/bin_debug_2015x64
          Debug:DESTDIR = d:/qwt-6.1.3/examples/bin_debug_2015x64
       } else {
         equals(MSVC_VER, 12.0) {
          Release:DESTDIR = d:/qwt-6.1.3/examples/bin_debug_2013x64
          Debug:DESTDIR = d:/qwt-6.1.3/examples/bin_debug_2013x64
         }
       }
   }
} if(!contains(QMAKE_HOST.arch, x86_64)) {

   win32-msvc* {
       MSVC_VER = $$(VisualStudioVersion)
       equals(MSVC_VER, 14.0){
          Release:DESTDIR = d:/qwt-6.1.3/examples/bin_debug_2015x64
          Debug:DESTDIR = d:/qwt-6.1.3/examples/bin_debug_2015x64
       } else {
         equals(MSVC_VER, 12.0) {
          Release:DESTDIR = d:/qwt-6.1.3/examples/bin_debug_2013x32
          Debug:DESTDIR = d:/qwt-6.1.3/examples/bin_debug_2013x32
         }
       }
   }

}

QT += network
QT += concurrent

QT += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = UDPReciver
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui
