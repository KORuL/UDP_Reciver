#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QUdpSocket>
#include <QIODevice>
#include <QTime>
#include <QSettings>
#include <QHostAddress>
#include <QEvent>

#include <QThreadPool>
#include <QThread>
#include <QtConcurrent>
#include <QHostAddress>
#include <QFile>
#include <QFileDialog>

//#include "form.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

    bool    start;
    int     idealThreadCnt;
    quint64 Allsize;
    QString STRFile;

    bool    byteOrder;
    bool    SaveTo_Filetrue_IPfalse;

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

protected:
    void closeEvent (QCloseEvent  *);

private Q_SLOTS:
    void PB_ConectionRecieve_clicked ();
    void PB_ConectionTransmit_clicked();

    void on_PB_Start_2_clicked();
    void on_PB_Stop_2_clicked ();

    void connectedR   ();
    void connectedT   ();
    void disconnectedR();
    void disconnectedT();

    void saveParam();
    void loadParam();

    void readyReadSlot();

    void errorR(QAbstractSocket::SocketError error);
    void errorT(QAbstractSocket::SocketError error);
    void Error (QString &err, QAbstractSocket::SocketError error);

    void SendToFile(int     size);
    void SendToIP  (int     size);

    void Log(QString str);

    void on_CB_Multicast_clicked  ();
    void on_PB_ChooseFile_clicked ();
    void on_CB_byteOrder_clicked();
    void on_RB_SendToIP_clicked   ();
    void on_RB_SaveFile_clicked   ();
    void on_PB_clear_clicked      ();

private:
    Ui::MainWindow *ui;

    QUdpSocket      *SocRecive;
    QUdpSocket      *SocTransmit;

    QByteArray IN;
    QByteArray OUT;

    int dstLen;

    QFuture<void>   *func;
    QFuture<void>   funcWrite;

    QFile file;
};

#endif // MAINWINDOW_H
